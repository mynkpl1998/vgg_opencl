#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import torch
import torch.nn as nn
from PIL import Image
import glob
from tensorboardX import SummaryWriter


# In[2]:


dataset_dir = "cifar/cifar/"
train_files = glob.glob(dataset_dir+"train/*")
test_files = glob.glob(dataset_dir+"test/*")
labels = ["dog", "frog", "automobile", "horse", "deer", "bird", "airplane", "cat", "truck", "ship"]
IMG_ROWS = 32
IMG_COLS = 32
IMG_CHANNELS = 3
writer = SummaryWriter(log_dir="runs/v2")


# In[3]:


#train_files


# In[4]:


def read_data(files_list):
    
    X, Y = [], []
    
    for file in files_list:
        
        img = np.array(Image.open(file))
        img_reshaped = np.stack((img[:, :, 0], img[:, :, 1], img[: ,:, 2]))
        
        for i, label in enumerate(labels):
            if label in file:
                Y.append(i)
            
        X.append(np.copy(img_reshaped))
        
    return (X, Y)


# # Load Train Data in Memory

# In[5]:


train_X, train_Y = read_data(train_files)
train_X = np.array(train_X)
train_Y = np.array(train_Y)
print(train_X.shape)
print(train_Y.shape)


# # Load Test Data in Memory

# In[6]:


test_X, test_Y = read_data(test_files)
test_X = np.array(test_X)
test_Y = np.array(test_Y)
print(test_X.shape)
print(test_Y.shape)


# # Pre-Processing of Images

# In[7]:


train_X = train_X/255.0
test_X = test_X/255.0


# # Define VGG-16 model here

# In[8]:


class vgg(nn.Module):
    
    def __init__(self):
        super(vgg, self).__init__()
        
        self.in_shape = IMG_ROWS
        self.in_channels = IMG_CHANNELS
        
        self.layer_names = ["c1","c2","c3","c4","c5","c6","c7","c8","c9","c10","c11","c12","c13","c14","c15","c16","f1","f2","f3"]
        self.c1 = nn.Conv2d(in_channels=self.in_channels, out_channels=64, kernel_size=3, padding=1)
        self.c2 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, padding=1)
        self.m1 = nn.MaxPool2d(kernel_size=(2,2))
        self.c3 = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3, padding=1)
        self.c4 = nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, padding=1)
        self.m2 = nn.MaxPool2d(kernel_size=(2,2))
        self.c5 = nn.Conv2d(in_channels=128, out_channels=256, kernel_size=3, padding=1)
        self.c6 = nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, padding=1)
        self.c7 = nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, padding=1)
        self.c8 = nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, padding=1)
        self.m3 = nn.MaxPool2d(kernel_size=(2,2))
        self.c9 = nn.Conv2d(in_channels=256, out_channels=512, kernel_size=3, padding=1)
        self.c10 = nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, padding=1)
        self.c11 = nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, padding=1)
        self.c12 = nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, padding=1)
        self.m4 = nn.MaxPool2d(kernel_size=(2,2))
        self.c13 = nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, padding=1)
        self.c14 = nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, padding=1)
        self.c15 = nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, padding=1)
        self.c16 = nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, padding=1)
        
        self.m5 = nn.MaxPool2d(kernel_size=(2,2))
        self.f1 = nn.Linear(in_features=512, out_features=512)
        self.f2 = nn.Linear(in_features=512, out_features=512)
        self.f3 = nn.Linear(in_features=512, out_features=10)
        
        self.dropout = nn.Dropout()
        
        self.activation = nn.ReLU()
    
    
    def forward(self, input_, bsize=1):
        
        out = self.activation(self.c1(input_))
        out = self.activation(self.c2(out))
        out = self.m1(out)
        out = self.activation(self.c3(out))
        out = self.activation(self.c4(out))
        out = self.m2(out)
        out = self.activation(self.c5(out))
        out = self.activation(self.c6(out))
        out = self.activation(self.c7(out))
        out = self.m3(out)
        out = self.activation(self.c8(out))
        out = self.activation(self.c9(out))
        out = self.activation(self.c10(out))
        out = self.m4(out)
        out = self.activation(self.c11(out))
        out = self.activation(self.c12(out))
        out = self.activation(self.c13(out))
        out = self.m5(out)
        out = out.view(bsize, -1) # Flatten
        out = self.activation(self.f1(out))
        out = self.dropout(out)
        out = self.activation(self.f2(out))
        out = self.dropout(out)
        out = self.f3(out)
        
        return out


# In[9]:


model = vgg()
#model = model.cuda()
model = model.eval()
model = model.cpu()


# In[10]:


print(model)
model.load_state_dict(torch.load('weights.torch'))
sum = 0
str_1 = open('weight_vgg_16.dat',"wb")
for k, v in model.state_dict().items():
    v = v.numpy()
    v = v.astype(np.float32)
    sum = sum+np.product(v.shape)
    print (np.product(v.shape), end=",")
    v = v.reshape(1,np.product(v.shape))
    v.tofile(str_1)
# # # Define Loss and Optimizer

# In[11]:


LR = 0.0001(0.5*(epoch//30))

criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=LR)
print(optimizer)


# # Training Statistics

# In[12]:


BATCH_SIZE = 64
MAX_EPOCHS = 300
local_batches = np.arange(0, len(train_X), BATCH_SIZE)


# In[ ]:


steps = 0

for epoch in range(0, MAX_EPOCHS):
    
    model.train()
    
    train_ids = np.arange(0, len(train_X))
    np.random.shuffle(train_ids)
    
    total_loss = 0.0
    
    for i in range(0, local_batches.shape[0]-1):
        
        steps += 1
        indexes = train_ids[local_batches[i]:local_batches[i+1]]
        
        local_batch_x = np.take(train_X, indices=indexes, axis=0)
        local_batch_x = torch.from_numpy(local_batch_x).float().cuda()
        
        local_batch_y = np.take(train_Y, indices=indexes, axis=0)
        local_batch_y = torch.from_numpy(local_batch_y).long().cuda()
        
        model_out = model.forward(local_batch_x, bsize=BATCH_SIZE)
        
        loss = criterion(model_out, local_batch_y)
        
        total_loss = loss.item()
        
        # make previous grad zero
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        
        print("Epoch: %d/%d, Batch : %d/%d, Loss : %.4f"%(epoch+1, MAX_EPOCHS, i+1, local_batches.shape[0]-1, loss.item()))       
        writer.add_scalar("step_loss", loss.item(), steps)
        
    writer.add_scalar("train_loss", total_loss/(i+1), epoch)


#

torch.save(model.state_dict(),"weights.torch")



# In[15]:


model = model.eval()
model = model.cpu()


# In[16]:


correct_count = 0

for i,test_sample in enumerate(test_X):
    
    input_ = torch.from_numpy(test_sample).float()
    model_out = model.forward(input_.view(1,3,32,32))
    prob = model_out.softmax(dim=1)
    
    model_predict = torch.argmax(prob)
    
    if model_predict.item() == test_Y[i]:
        correct_count += 1
    
    print("Sample : %d, Accuracy : %.2f"%(i+1, (correct_count/(i+1))))


# In[21]:


correct_count / 10000 * 100

